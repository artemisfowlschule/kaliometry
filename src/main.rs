use std::io::{stdin, stdout, Write};
use std::ops::{Add, Div, Mul, Sub};

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd)]
struct Point {
    x: f64,
    y: f64,
}
impl Point {
    fn new(x: f64, y: f64) -> Self {
        Self { x, y }
    }

    fn distance(&self, other: &Self) -> f64 {
        let base_distance = *self - *other;
        (base_distance.x.powf(2.0) + base_distance.y.powf(2.0)).sqrt()
    }
}
impl Add<Point> for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self::new(self.x + other.x, self.y + other.y)
    }
}
impl Sub<Point> for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self::new(self.x - other.x, self.y - other.y)
    }
}
impl Mul<f64> for Point {
    type Output = Self;

    fn mul(self, other: f64) -> Self::Output {
        Self::new(self.x * other, self.y * other)
    }
}
impl Div<f64> for Point {
    type Output = Self;

    fn div(self, other: f64) -> Self::Output {
        Self::new(self.x / other, self.y / other)
    }
}

struct VerticalLine {
    x_intercept: f64,
}
impl VerticalLine {
    fn new(x: f64) -> Self {
        Self { x_intercept: x }
    }

    fn intersect(&self, other: &Line) -> Point {
        other.calculate_point(self.x_intercept)
    }
}
impl From<Point> for VerticalLine {
    fn from(point: Point) -> Self {
        Self {
            x_intercept: point.x,
        }
    }
}

struct Line {
    slope: f64,
    intersect: f64,
}
impl Line {
    fn new(slope: f64, intersect: f64) -> Self {
        Self { slope, intersect }
    }

    fn calculate_y(&self, x: f64) -> f64 {
        self.slope * x + self.intersect
    }

    fn calculate_point(&self, x: f64) -> Point {
        Point::new(x, self.calculate_y(x))
    }

    fn intersect(&self, other: &Self) -> Point {
        let slope_delta = (self.slope - other.slope).abs();
        let y_intersect_delta = (self.intersect - other.intersect).abs();
        let x = y_intersect_delta / slope_delta;
        self.calculate_point(x)
    }
}

struct Triangle {
    a: Point,
    b: Point,
    c: Point,
}
impl Triangle {
    fn new(a: Point, b: Point, c: Point) -> Self {
        Self { a, b, c }
    }

    fn area(&self) -> f64 {
        let ab = self.a.distance(&self.b);
        let ac = self.a.distance(&self.c);
        let bc = self.b.distance(&self.c);
        let s = (ab + ac + bc) / 2.0;
        0.25 * (s * (s - ab) * (s - ac) * (s - bc)).sqrt()
    }
}

fn precision_f64(x: f64, decimals: u8) -> f64 {
    if x == 0. || decimals == 0 {
        0.
    } else {
        let shift = decimals as i32; // - x.abs().log10().ceil() as i32;
        let shift_factor = 10_f64.powi(shift);

        (x * shift_factor).round() / shift_factor
    }
}

fn brute_force(
    lower_line: &Line,
    upper_line: &Line,
    intersect_line: &Line,
    precision: u8,
) -> VerticalLine {
    let lower_point = lower_line.intersect(intersect_line);
    let upper_point = upper_line.intersect(intersect_line);
    let delta = 10.0_f64.powf(-(precision as f64));
    let (mut current_x, other_x) = {
        if upper_point.x > lower_point.x {
            (lower_point.x, upper_point.x)
        } else {
            (upper_point.x, lower_point.x)
        }
    };

    loop {
        if current_x > other_x {
            panic!("x of {current_x} too large compared to {other_x}")
        }

        let vertical = VerticalLine::new(current_x);
        current_x += delta;
        let lower_vertical_point = vertical.intersect(lower_line);
        let upper_vertical_point = vertical.intersect(upper_line);
        let middle_vertical_point = vertical.intersect(intersect_line);

        let lower_triangle =
            Triangle::new(lower_point, lower_vertical_point, middle_vertical_point);
        let upper_triangle =
            Triangle::new(upper_point, upper_vertical_point, middle_vertical_point);

        let lower_triangle_area = precision_f64(lower_triangle.area(), precision);
        let upper_triangle_area = precision_f64(upper_triangle.area(), precision);

        if lower_triangle_area == upper_triangle_area {
            return vertical;
        }
    }
}

fn input(message: &str) -> f64 {
    let mut input = String::new();

    print!("{message}");
    stdout().flush().expect("Flusing did not work");

    stdin().read_line(&mut input).expect("Failed to read line");
    input.trim().parse().expect("Failed to parse as a number")
}

fn create_new_line(line: &str) -> Line {
    let slope = input(&format!("Please enter the slope for the {line}: "));
    let y_intercept = input(&format!("Please enter the y intercept for the {line}: "));
    Line::new(slope, y_intercept)
}

fn main() {
    loop {
        let lower_line = create_new_line("first line");
        let upper_line = create_new_line("second line");
        let inbetween_line = create_new_line("line inbetween");
        let precision = input("Please enter the precision to brute force this with: ") as u8;

        let vertical = brute_force(&lower_line, &upper_line, &inbetween_line, precision);
        let lower = precision_f64(vertical.intersect(&lower_line).y, precision);
        let upper = precision_f64(vertical.intersect(&upper_line).y, precision);
        println!("X: {} First: {lower} Second: {upper}", vertical.x_intercept);
    }
}
